import {DemoClass} from './DemoClass';
import {AnotherClass} from './AnotherClass';

const instance_1 = new DemoClass();
const instance_2 = new AnotherClass({className:(<any>AnotherClass).name}); 
export class DemoClass {
    public data:any;

    constructor(parameters: { data: string; }) {
        console.log("DemoClass " + JSON.stringify(parameters));
        this.data = parameters;
    }

    getData() {
        return JSON.stringify(this.data);
    }
}
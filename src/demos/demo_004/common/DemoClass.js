"use strict";
exports.__esModule = true;
var DemoClass = (function () {
    function DemoClass(parameters) {
        console.log("DemoClass " + JSON.stringify(parameters));
        this.data = parameters;
    }
    DemoClass.prototype.getData = function () {
        return JSON.stringify(this.data);
    };
    return DemoClass;
}());
exports.DemoClass = DemoClass;
//# sourceMappingURL=DemoClass.js.map
"use strict";
exports.__esModule = true;
var DemoClass_1 = require("./common/DemoClass");
var ThirdClass_1 = require("./common/ThirdClass");
var AnotherClass_1 = require("./common/AnotherClass");
var instance_1 = new DemoClass_1.DemoClass({ name: "Instance" });
var instance_2 = new AnotherClass_1.AnotherClass({ name: "AnotherInstance" });
var instance_3 = new ThirdClass_1.ThirdClass({ name: "ThirdClassInstance" });
//# sourceMappingURL=index.js.map
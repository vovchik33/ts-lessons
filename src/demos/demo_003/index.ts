import {DemoClass} from "./common/DemoClass";
import {ThirdClass} from "./common/ThirdClass";
import {AnotherClass} from "./common/AnotherClass";

const instance_1 = new DemoClass({name: "Instance"});
const instance_2 = new AnotherClass({name: "AnotherInstance"});
const instance_3 = new ThirdClass({name: "ThirdClassInstance"});
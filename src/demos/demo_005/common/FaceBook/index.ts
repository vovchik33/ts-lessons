import {Person, SocialNetworks} from "../../interfaces";

export class FaceBook implements SocialNetworks{
    private persons: Person[];
    // @ts-ignore
    constructor(parameters) {
        console.log("FaceBook " + JSON.stringify(parameters));
        this.persons = parameters;
    }

    getUser(): Person {
        return this.persons[0];
    }
}